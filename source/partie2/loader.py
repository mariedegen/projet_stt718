import pandas as pd
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn import preprocessing
import numpy as np

def load_data(name):
    data = pd.read_csv("HTRU_2.csv")
    predictor = data.iloc[:,0:8]
    labels = data.iloc[:,8:9]
    return data,predictor,labels

def split(data,labels):
    sss = StratifiedShuffleSplit(n_splits=5, test_size=0.2, random_state=0)
    for train_index, test_index in sss.split(data, labels):
        x_train, x_test = data.values[train_index], data.values[test_index]
        y_train , y_test = labels.values[train_index], labels.values[test_index]
    return  x_train, x_test, y_train , y_test


def load_normalize(name):
    data = pd.read_csv("HTRU_2.csv")
    predictor = data.iloc[:,0:8]
    labels = data.iloc[:,8:9]
    predictor = preprocessing.scale(predictor)
    return data,predictor,labels

def split_normalize(data,labels):
    sss = StratifiedShuffleSplit(n_splits=5, test_size=0.2, random_state=0)
    for train_index, test_index in sss.split(data, labels):
        x_train, x_test = data[train_index], data[test_index]
        y_train , y_test = labels.values[train_index], labels.values[test_index]
    return  x_train, x_test, y_train , y_test
        