import pandas as pd
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.impute import SimpleImputer
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from loader import *
from sklearn.metrics import roc_auc_score
from sklearn.svm import SVC
from sklearn.metrics import confusion_matrix
from sklearn.metrics import recall_score
from imblearn.over_sampling import SMOTE


df,X,y = load_data("HTRU_2.csv")
y = df.y
X = preprocessing.scale(X)
x_train, x_test, y_train , y_test = split_normalize(X,y)

smt = SMOTE()
x_train, y_train = smt.fit_sample(x_train, y_train)

clf = RandomForestClassifier(n_estimators=100)

# Training
clf.fit(x_train, y_train)

"""Prediction with training value"""
pred_t = clf.predict(x_train)
print("test accuracy",accuracy_score(y_train, pred_t))


# prediction with testvalue
pred_y = clf.predict(x_test)
print(pred_y)


print("test accuracy",accuracy_score(y_test, pred_y) )

prob_y_4 = clf.predict_proba(x_test)
prob_y_4 = [p[1] for p in prob_y_4]
print("Auroc",roc_auc_score(y_test, prob_y_4) )


confusion_matrix = confusion_matrix(y_test, pred_y)
print(confusion_matrix)


print("recall", recall_score(y_test,pred_y))
