import pandas as pd
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.impute import SimpleImputer
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from loader import *
from sklearn.metrics import roc_auc_score
from sklearn.svm import SVC
from sklearn.metrics import confusion_matrix
from sklearn.metrics import recall_score
from sklearn.linear_model import LogisticRegression
from imblearn.over_sampling import SMOTE
from imblearn.under_sampling import NearMiss
from sklearn.feature_selection import SelectFromModel
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.linear_model import RidgeClassifier
from sklearn.linear_model import RidgeClassifierCV
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import f1_score
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegressionCV
from sklearn.neighbors import KNeighborsClassifier



df,X,y = load_data("HTRU_2.csv")
y = df.y
print("Nombre de 1", np.sum(y))
X = preprocessing.scale(X)
print("X shape",X.shape)
x_train, x_test, y_train , y_test = split_normalize(X,y)

#print(np.sum(y_train + np.sum(y_test))

#print(x_test,x_test.shape)
#print(x_train,x_train.shape)


#python3
# clf = LogisticRegression(class_weight='balanced')

def compute(x_test,x_train,y_test,y_train,clf):
    # Training
    clf.fit(x_train, y_train)
    pred_t = clf.predict(x_train)
    print("train accuracy",accuracy_score(y_train, pred_t))


    # prediction with testvalue
    pred_y = clf.predict(x_test)
    print(np.unique(pred_y))


    print("test accuracy",accuracy_score(y_test, pred_y) )

    #prob_y_4 = clf.predict_proba(x_test)
    #prob_y_4 = [p[1] for p in prob_y_4]
    #print("Auroc",roc_auc_score(y_test, prob_y_4) )


    #confusion_matrix = confusion_matrix(y_test, pred_y)
    #print(confusion_matrix)


    print("recall", recall_score(y_test,pred_y))

    print("f1_score",f1_score(y_test,pred_y))




"""
compute(x_test,x_train,y_test,y_train,clf)
#model = SelectFromModel(clf, prefit=True)
#x_new = model.transform(x_train)
#model = ExtraTreesClassifier()
#model.fit(x_train, y_train)
#print(model.feature_importances_)


clf = RidgeClassifier(class_weight='balanced')
compute(x_test,x_train,y_test,y_train,clf)


print("0.02")
clf = RidgeClassifier(class_weight='balanced', alpha = 0.03)
compute(x_test,x_train,y_test,y_train,clf)


#param_grid = {'alphas' : [1e-3, 1e-2, 1e-1, 1]}
#clf =  knn_gscv = GridSearchCV(clf, param_grid, cv=5)
#compute(x_test,x_train,y_test,y_train,clf)
#cl.fit(x_train,y_train)


#clf = RidgeClassifierCV(alphas=[1e-3, 1e-2, 1e-1, 1],class_weight='balanced')
#compute(x_test,x_train,y_test,y_train,clf)

#from sklearn.feature_selection import VarianceThreshold
#sel = VarianceThreshold(threshold=(.8 * (1 - .8)))
#print(sel.fit_transform(x_train).shape)
#print(x_train.shape)
"""
"""print("Ridge after")
model = SelectFromModel(clf,prefit=True)
train = model.transform(x_train)
print(train.shape)
test = model.transform(x_test)
compute(test,train,y_test,y_train,clf)


print("Random Forest")
clf = RandomForestClassifier(class_weight='balanced',n_estimators =100)
compute(x_test,x_train,y_test,y_train,clf)

print("Random Forest entropy")
clf = RandomForestClassifier(class_weight='balanced',n_estimators =100,criterion='entropy')
compute(x_test,x_train,y_test,y_train,clf)

print("Random Forest After")

model = SelectFromModel(clf,prefit=True)
train = model.transform(x_train)
print(train.shape)
test = model.transform(x_test)
compute(test,train,y_test,y_train,clf)


from sklearn.linear_model import RidgeClassifier
clf = RidgeClassifier(class_weight='balanced')"""

#parameters = {'criterion':('entropy','gini'),'max_features':('sqrt','log2'),'bootstrap':(True,False)}
#clf = RandomForestClassifier(class_weight='balanced_subsample',n_estimators =100)

parameters = [{'kernel': ['rbf'], 'gamma': [1e-1, 1e-2, 1e-3, 1e-4],
                     'C': [1, 10, 100, 500, 1000]},
                    {'kernel': ['linear'], 'C': [1, 10, 100, 1000]}]

#clf = DecisionTreeClassifier(class_weight="balanced")
clf = SVC(probability=True)
#a = np.arange(9)/10
#a[a==0.]=1.
#parameters = {'C': a,'penalty':(str)}
#clf = LogisticRegression(class_weight='balanced')
#gc = LogisticRegressionCV(cv=5,class_weight='balanced')




"""clf = KNeighborsClassifier()
parameters = {'n_neighbors': np.arange(1, 25)}"""

gc = GridSearchCV(clf, parameters, cv=5)
compute(x_test,x_train,y_test,y_train,gc)

"""
clf = KNeighborsClassifier(n_neighbors=11,1.)
#clf.fit(x_train,y_train)
compute(x_test,x_train,y_test,y_train,clf)"""


model = SelectFromModel(clf,prefit=False)
model.fit(x_train,y_train)
train = model.transform(x_train)
print("Après selection", train.shape)
test = model.transform(x_test)
compute(test,train,y_test,y_train,clf)


