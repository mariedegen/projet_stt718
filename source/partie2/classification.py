import pandas as pd
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.impute import SimpleImputer
import numpy as np

data = pd.read_csv("HTRU_2.csv")
#print(data.head())
#print(data.index)
#print(data.describe())
#print(data['y'])
#labels = data['y']
print(data['y'].value_counts())
predictor = data.iloc[:,0:8]
#print(predictor)
labels = data.iloc[:,8:9]
#print(labels)

sss = StratifiedShuffleSplit(n_splits=5, test_size=0.2, random_state=0)
#print(data.shape)
for train_index, test_index in sss.split(data, labels):
    x_train, x_test = data.values[train_index], data.values[test_index]
    y_train , y_test = labels.values[train_index], labels.values[test_index]

#labels = data[data.columns[8:9]]
#print(labels)
#data = data[data.columns[0:8]]
#sss = StratifiedShuffleSplit(n_splits=1, test_size=0.2, random_state=0)
#sss.get_n_splits(data, labels)
"""for train_index, test_index in sss.split(data, labels):
    X_train, X_test = data[train_index], data[test_index]
    y_train, y_test = labels[train_index], labels[test_index]"""

#print(x_train)
#print(y_train)