from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from loader import *
from sklearn.metrics import roc_auc_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import recall_score
from imblearn.over_sampling import SMOTE
from imblearn.under_sampling import NearMiss
from sklearn.model_selection import GridSearchCV


df,X,y = load_data("HTRU_2.csv")
y = df.y
X = preprocessing.scale(X)
print("X shape",X.shape)
x_train, x_test, y_train , y_test = split_normalize(X,y)

print(x_test,x_test.shape)
print(x_train,x_train.shape)


clf = KNeighborsClassifier()
param_grid = {'n_neighbors': np.arange(1, 25)}
cl =  knn_gscv = GridSearchCV(clf, param_grid, cv=5)
cl.fit(x_train,y_train)

print(cl.best_params_)

# Training
clf = KNeighborsClassifier(n_neighbors=cl.best_params_['n_neighbors'])
clf.fit(x_train, y_train)

"""Prediction with training value"""
pred_t = clf.predict(x_train)
print("train accuracy",accuracy_score(y_train, pred_t))


# prediction with testvalue
pred_y = clf.predict(x_test)
print(pred_y)


print("test accuracy",accuracy_score(y_test, pred_y) )

prob_y_4 = clf.predict_proba(x_test)
prob_y_4 = [p[1] for p in prob_y_4]
print("Auroc",roc_auc_score(y_test, prob_y_4) )


#confusion_matrix = confusion_matrix(y_test, pred_y)
#print(confusion_matrix)


print("recall", recall_score(y_test,pred_y))


""" Using SMOTE"""

smt = SMOTE()
x_train, y_train = smt.fit_sample(x_train, y_train)


clf.fit(x_train, y_train)

"""Prediction with training value"""
pred_t = clf.predict(x_train)
print("train accuracy",accuracy_score(y_train, pred_t))


# prediction with testvalue
pred_y = clf.predict(x_test)
print(pred_y)


print("test accuracy s",accuracy_score(y_test, pred_y) )

prob_y_4 = clf.predict_proba(x_test)
prob_y_4 = [p[1] for p in prob_y_4]
print("Auroc s",roc_auc_score(y_test, prob_y_4) )


#confusion_matrix = confusion_matrix(y_test, pred_y)
#print(confusion_matrix)


print("recall s", recall_score(y_test,pred_y))




""" Near miss """
nr = NearMiss()
x_train, y_train = nr.fit_sample(x_train, y_train)
clf.fit(x_train, y_train)

"""Prediction with training value"""
pred_t = clf.predict(x_train)
print("train accuracy N",accuracy_score(y_train, pred_t))


# prediction with testvalue
pred_y = clf.predict(x_test)
print(pred_y)


print("test accuracy s",accuracy_score(y_test, pred_y) )

prob_y_4 = clf.predict_proba(x_test)
prob_y_4 = [p[1] for p in prob_y_4]
print("Auroc N",roc_auc_score(y_test, prob_y_4) )


#confusion_matrix = confusion_matrix(y_test, pred_y)
#print(confusion_matrix)


print("recall N", recall_score(y_test,pred_y))
